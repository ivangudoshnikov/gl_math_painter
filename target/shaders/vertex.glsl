#version 150 core

in vec4 in_Position;
in vec3 in_Normal;
in vec4 in_Ambient;
in vec4 in_Diffuse;
in vec4 in_Specular;
in float in_Shininess;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec4 pass_Position;
out vec3 pass_Normal;
out vec4 pass_Ambient;
out vec4 pass_Diffuse;
out vec4 pass_Specular;
out float pass_Shininess;

void main(void) {
    pass_Position=view * model * in_Position;
    gl_Position = projection*pass_Position;    
    pass_Normal=in_Normal;    
    pass_Ambient=in_Ambient;
    pass_Diffuse=in_Diffuse;
    pass_Specular=in_Specular;
    pass_Shininess=in_Shininess;	
}