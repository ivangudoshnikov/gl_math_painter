#version 150 core

in vec4 pass_Position;
in vec3 pass_Normal;
in vec4 pass_Ambient;
in vec4 pass_Diffuse;
in vec4 pass_Specular;
in float pass_Shininess;

const int maxLights=10;

uniform mat3 normalTransMatrix;
uniform int lightsNumber;
uniform float lightsArray[maxLights*2]; //2 vectors per lightsource

out vec4 out_Color;

vec4 Lightning(const in vec3 lightDirection, in vec3 normal, const in vec3 dirPosToEye,const in vec4 lightColor,const in vec4 diffuse,const in vec4 specular,const in float shininess){
if (dot(normal,lightDirection)<0){ //Two-sided
    normal=-normal;
}    

if (dot(normal,dirPosToEye)<0){
    normal=-normal;
}

float cosAngleLamb=dot(normal,lightDirection);
    vec4 lambert = diffuse*lightColor*max(cosAngleLamb,0.0);

    float cosAnglePhong=dot(normal,normalize(lightDirection+dirPosToEye));
    vec4 phong=specular*lightColor*pow(max(cosAnglePhong,0.0),shininess);
    
    return lambert+phong;
}

void main(void) {
    vec3 lightDirection;    
    vec3 normal=normalize(normalTransMatrix*pass_Normal);
    vec4 result=pass_Ambient;    
    
    vec3 dirPosToEye=normalize(-pass_Position.xyz/pass_Position.w);
    
    for(int i=0; i<lightsNumber;i++){
        vec4 lightPos = vec4(lightsArray[8*i],lightsArray[8*i+1],lightsArray[8*i+2],lightsArray[8*i+3]);
        vec4 lightCol = vec4(lightsArray[8*i+4],lightsArray[8*i+5],lightsArray[8*i+6],lightsArray[8*i+7]);

        if(lightPos.w==0){ 
            lightDirection=normalize(lightPos.xyz);        
        }else{
            lightDirection=normalize(lightPos.xyz/lightPos.w-pass_Position.xyz/pass_Position.w); //lightsource.coord-fragment.coord            
        }
        result+=Lightning(lightDirection,normal,dirPosToEye,lightCol,pass_Diffuse, pass_Specular, pass_Shininess);        
    }
    out_Color=result;    
}