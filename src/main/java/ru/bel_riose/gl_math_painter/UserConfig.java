/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 *
 * @author Bel_Riose
 */
public class UserConfig {

    public Function function;
    public float step;
    public LightSource[] lights;
    public GlCamera glCamera;

    public UserConfig() throws BadRectangleException, IntersectingDomainException {
        glCamera=new GlCamera(60f, (float)1280 / 1024, 0.1f, 100f,
                new Vector3f(0, 0, 5), new Vector3f(0, 1, 0), new Vector3f(0, 0, 0));
        
        Rectangle[] square = {new Rectangle(-2, true, 2, true, -4, true, 4, true)};
        
        Function quad = new Function(square,false) {
            @Override
            public Material getMaterial(float u, float v) {
                Material result = new Material();
                result.ambient = new Vector4f(0.1f,0.1f,0.1f, 1f);
                result.diffuse = new Vector4f(0.7f, 0f, 0.7f, 1f);
                result.specular = new Vector4f(1f, 1f, 1f, 1f);
                result.shininess = 100;
                return result;                
            }
            
            @Override
            public Vector4f getCoords(float u, float v) {
                return new Vector4f(u, v, 0, 1);
            }
            
            @Override
            Vector3f uDerivative(float u, float v) {
                return new Vector3f(1, 0, 0);
            }
            
            @Override
            Vector3f vDerivative(float u, float v) {
                return new Vector3f(0, 1, 0);
            }
        };
        
        Function fold = new Function(square,false) {
            @Override
            public Vector4f getCoords(float u, float v) {
                return new Vector4f(u, v, u * u * u + u * v, 1);
            }

            @Override
            Vector3f uDerivative(float u, float v) {
                return new Vector3f(1, 0, 3 * u * u + v);
            }

            @Override
            Vector3f vDerivative(float u, float v) {
                return new Vector3f(0, 1, u);
            }

            @Override
            public Material getMaterial(float u, float v) {
                Material result = new Material();
                result.ambient = new Vector4f(0.1f, 0.1f, 0.1f, 1f);
                result.diffuse = new Vector4f(Math.abs(u) / 2f, Math.abs(v) / 2f, 1, 1f);
                result.specular = new Vector4f(1f, 1f, 1f, 1f);
                result.shininess = 100;
                return result;
            }
        };


        Rectangle[] fourSquares = {new Rectangle(-2, true, 0, false, -2, true, 0, false),
            new Rectangle(0, false, 2, true, -2, true, 0, false),
            new Rectangle(-2, true, 0, false, 0, false, 2, true),
            new Rectangle(0, false, 2, true, 0, false, 2, true)};

        Rectangle[] fourDistantSquares = {new Rectangle(-2, true, -0.5f, true, -2, true, -0.5f, true),
            new Rectangle(0.5f, true, 2, true, -2, true, -0.5f, true),
            new Rectangle(-2, true, -0.5f, true, 0.5f, true, 2, true),
            new Rectangle(0.5f, true, 2, true, 0.5f, true, 2, true)};


        Function hyperboloid = new Function(fourSquares,false) {
            @Override
            public Vector4f getCoords(float u, float v) {
                return new Vector4f(u, v, 1 / (u * v), 1);
            }

            @Override
            Vector3f uDerivative(float u, float v) {
                return new Vector3f(1, 0, 3 * u * u + v);
            }

            @Override
            Vector3f vDerivative(float u, float v) {
                return new Vector3f(0, 1, u);
            }

            @Override
            public Material getMaterial(float u, float v) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        Rectangle[] squareSphere = {new Rectangle(-(float)Math.PI/2, true, (float) Math.PI/2, true, 0, true, 2*(float)Math.PI, true)};
        Function sphere = new Function(squareSphere,true) {
            @Override
            public Material getMaterial(float u, float v) {
                Material result = new Material();
                result.ambient = new Vector4f(0.1f, 0.1f, 0.1f, 1f);
                result.diffuse = new Vector4f((float)((u+Math.PI/2)/Math.PI),(float)(v/(2*Math.PI)), 1, 1f);
                result.specular = new Vector4f(1f, 1f, 1f, 1f);
                result.shininess = 200;
                return result;
            }
            
            @Override
            public Vector4f getCoords(float u, float v) {
                return new Vector4f((float)(Math.cos(u)*Math.cos(v)),(float)(Math.cos(u)*Math.sin(v)), (float)Math.sin(u), 1);
            }
            
            @Override
            Vector3f uDerivative(float u, float v) {
                return new Vector3f((float)(-Math.sin(u)*Math.cos(v)), (float)(-Math.sin(u)*Math.sin(v)), (float)Math.cos(u));
            }
            
            @Override
            Vector3f vDerivative(float u, float v) {
                return new Vector3f((float)(-Math.cos(u)*Math.sin(v)), (float)(Math.cos(u)*Math.cos(v)), 0);
            }
        };
        
        
        step = 0.01f;
        //lights =new LightSource[]{new LightSource(-5, 0, 0, 1, 0.5f, 0.5f, 0.5f, 1)};
        lights =new LightSource[]{new LightSource(-5, 0, 0, 1, 0.7f, 0.7f, 0.7f, 1),new LightSource(5, 0, 0, 1, 0.3f, 0.3f, 0.3f, 1)};
        //lights =new LightSource[]{new LightSource(0, 0, 5, 1, 0.5f, 0.5f, 0.5f, 1),new LightSource(0, 0, -10, 1, 1, 1, 1f, 1)};
        function=fold;

    }
}
