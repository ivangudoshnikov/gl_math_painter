/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import org.lwjgl.util.vector.*;

/**
 *
 * @author Bel_Riose
 */
class IntersectingDomainException extends Exception {
}

public abstract class Function {

    public static class DataSizes {
        // The amount of bytes an number value has
        public static final int numberBytes = 4;
        
        // The amount of numbers that a vertex has for position
        public static final int positionNumbersCount = 4;
        //position offset
        public static final int positionByteOffset = 0;
        
        // The amount of numbers that a vertex has for normal
        public static final int normalNumbersCount = 3;
        //normal offset
        public static final int normalByteOffset = numberBytes * positionNumbersCount;
        
        public static final int ambientNumbersCount = 4;
        public static final int ambientByteOffset = numberBytes * (positionNumbersCount+normalNumbersCount);
        
        public static final int diffuseNumbersCount = 4;
        public static final int diffuseByteOffset = numberBytes * (positionNumbersCount+normalNumbersCount+ambientNumbersCount);
        
        public static final int specularNumbersCount = 4;
        public static final int specularByteOffset = numberBytes * (positionNumbersCount+normalNumbersCount+ambientNumbersCount+diffuseNumbersCount);
        
        public static final int shininessNumbersCount = 1;
        public static final int shininessByteOffset = numberBytes * (positionNumbersCount+normalNumbersCount+ambientNumbersCount+diffuseNumbersCount+specularNumbersCount);
        
        
        // The amount of elements that a vertex has totally
        public static final int totalNumbersCount = positionNumbersCount + normalNumbersCount+ambientNumbersCount+diffuseNumbersCount+specularNumbersCount+shininessNumbersCount;
        // The size of a vertex in bytes
        public static final int totalSizeInBytes = numberBytes * totalNumbersCount;
    }
    
    public Rectangle[] domain;

    public Function(Rectangle[] domain, boolean flipNormals) throws IntersectingDomainException {
        this.flipNormals=flipNormals;
        for (int i = 0; i < domain.length; i++) {
            for (int j = 0; i < domain.length; i++) {
                if ((i != j) && 
                        (((domain[i].x0_true<=domain[j].x0_true)&&(domain[j].x0_true<domain[i].x1_true)&&(domain[i].y0_true<=domain[j].y0_true)&&(domain[j].y0_true<domain[i].y1_true))||
                        ((domain[i].x0_true<=domain[j].x0_true)&&(domain[j].x0_true<domain[i].x1_true)&&(domain[i].y0_true<domain[j].y1_true)&&(domain[j].y1_true<=domain[i].y1_true))||
                        ((domain[i].x0_true<domain[j].x1_true)&&(domain[j].x1_true<=domain[i].x1_true)&&(domain[i].y0_true<=domain[j].y0_true)&&(domain[j].y0_true<domain[i].y1_true))||
                        ((domain[i].x0_true<domain[j].x1_true)&&(domain[j].x1_true<=domain[i].x1_true)&&(domain[i].y0_true<domain[j].y1_true)&&(domain[j].y1_true<=domain[i].y1_true)))) {
                    throw new IntersectingDomainException();
                }
            }
        }

        this.domain = domain;
    }
    private boolean flipNormals;
    public abstract Material getMaterial(float u, float v);
    
    public abstract Vector4f getCoords(float u, float v);

    abstract Vector3f uDerivative(float u, float v);

    abstract Vector3f vDerivative(float u, float v);

    public Vector3f getNormal(float u, float v) {
        Vector3f normal = new Vector3f();
        Vector3f.cross(uDerivative(u, v), vDerivative(u, v), normal);
        normal.normalise();
        if (flipNormals) normal.negate();
        return normal;
    }

    public float[] getData(float u, float v) {
        Vector4f coords = getCoords(u, v);
        Vector3f normal = getNormal(u, v);
        Material material = getMaterial(u, v);
        return new float[]{coords.x, coords.y, coords.z, coords.w,
            normal.x,normal.y, normal.z,
        material.ambient.x,material.ambient.y,material.ambient.z,material.ambient.w,
        material.diffuse.x,material.diffuse.y,material.diffuse.z,material.diffuse.w,
        material.specular.x,material.specular.y,material.specular.z,material.specular.w,
        material.shininess};
    }
}
