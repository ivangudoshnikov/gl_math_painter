/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import org.lwjgl.util.vector.*;

/**
 *
 * @author Bel_Riose
 */
public class MathUtils {
    /**Modifies and returns the given matrix!
     * @param a
     * @param m
     * @return 
     */
    public static Matrix3f multToNumber(float a, Matrix3f m){
        m.m00*=a;
        m.m10*=a;
        m.m20*=a;
        m.m01*=a;
        m.m11*=a;
        m.m21*=a;
        m.m02*=a;
        m.m12*=a;
        m.m22*=a;
        return m;
    }
    
    public static Matrix3f aStar(Vector3f v){
        Matrix3f result=new Matrix3f();
        result.m00=0;
        result.m10=-v.z;
        result.m20=v.y;
        result.m01=v.z;
        result.m11=0;
        result.m21=-v.x;
        result.m02=-v.y;
        result.m12=v.x;
        result.m22=0;
        return result;                
    }
    
    public static Matrix3f aaT(Vector3f v){
        Matrix3f result=new Matrix3f();
        result.m00=v.x*v.x;
        result.m10=v.x*v.y;
        result.m20=v.x*v.z;
        result.m01=v.x*v.y;
        result.m11=v.y*v.y;
        result.m21=v.y*v.z;
        result.m02=v.x*v.z;
        result.m12=v.y*v.z;
        result.m22=v.z*v.z;
        return result;
    }
    
    public static Matrix3f rotation3d(Vector3f axis, float t){
        Matrix3f result=new Matrix3f();
        Vector3f norm_axis=  new Vector3f();
        axis.normalise(norm_axis);
        Matrix3f.add(multToNumber((float)Math.cos(t), result),
                multToNumber(1f-(float)Math.cos(t), aaT(norm_axis)) , result);
        Matrix3f.add(result, multToNumber((float)Math.sin(t), aStar(norm_axis)), result);    
        return result;
    }
    
    public static Matrix3f normalTransform(Matrix4f m){
        Matrix3f result= new Matrix3f();
        result.m00=m.m00;
        result.m10=m.m10;
        result.m20=m.m20;
        result.m01=m.m01;
        result.m11=m.m11;
        result.m21=m.m21;        
        result.m02=m.m02;
        result.m12=m.m12;
        result.m22=m.m22;
        result.invert();
        result.transpose();
        return result;
    }
}
