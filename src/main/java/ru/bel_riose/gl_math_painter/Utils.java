/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

/**
 *
 * @author Bel_Riose
 */
public class Utils {

    public static float degreesToRadians(float degrees) {
        return degrees * (float) (Math.PI / 180d);
    }
}
