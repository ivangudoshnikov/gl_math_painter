/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.*;

/**
 *
 * @author Bel_Riose
 */

public class GlRenderer {
   
    private String windowTitle;
    private int displayWidth, displayHeight;
    public GlCamera glCamera;
    private LightSource[] lights;
    private MeshCreator meshCreator;
    // Quad variables
    private int vaoId = 0;
    private int vboId = 0;
    private int vboiId = 0;     
    
    // Shader variables
    private int vsId = 0;
    private int fsId = 0;
    private int pId = 0;
    // Matricies    
    private int projectionMatrixLocation = 0;    
    private int viewMatrixLocation = 0;
    private int modelMatrixLocation = 0;
    private int normalTransformMatrixLocation = 0;
    
    //lights uniform locations
    private int lightsNumberLocation=0;
    private int lightsLocation=0;
        

    public GlRenderer(String windowTitle) {
        this.windowTitle = windowTitle;
    }

    public void init(GlCamera glCamera,Function f, float step,LightSource[] lights) {        
        setupDisplay();
        this.glCamera=glCamera;
        this.lights=lights;
        setupMesh(f,step);
        setupShaders();
    }

    
    public void render() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        
        GL20.glUseProgram(pId);
              
        FloatBuffer matrix4Buffer = BufferUtils.createFloatBuffer(16);
        FloatBuffer matrix3Buffer = BufferUtils.createFloatBuffer(9);
        
        //Model matrix - now just identity
        Matrix4f modelMatrix = new Matrix4f();        
        modelMatrix.store(matrix4Buffer);
        matrix4Buffer.flip();
        GL20.glUniformMatrix4(modelMatrixLocation, false, matrix4Buffer);
        
        glCamera.getView().store(matrix4Buffer);
        matrix4Buffer.flip();
        GL20.glUniformMatrix4(viewMatrixLocation, false, matrix4Buffer);
        
        glCamera.getProjection().store(matrix4Buffer);
        matrix4Buffer.flip();
        GL20.glUniformMatrix4(projectionMatrixLocation, false, matrix4Buffer);
        
        Matrix4f modelViewMatrix= new Matrix4f();
        Matrix4f.mul(glCamera.getView(), modelMatrix, modelViewMatrix);
        Matrix3f normalTransMatrix = MathUtils.normalTransform(modelViewMatrix);
        normalTransMatrix.store(matrix3Buffer);
        matrix3Buffer.flip();
        GL20.glUniformMatrix3(normalTransformMatrixLocation, false, matrix3Buffer);
       
        //sending lights to the shaders
        GL20.glUniform1i(lightsNumberLocation, lights.length);        
        FloatBuffer lightsBuffer= BufferUtils.createFloatBuffer(lights.length*LightSource.numberCount);        
        for (int i =0;i<lights.length; i++ ){
            
            lightsBuffer.put(lights[i].getTransformedData(glCamera.getView()));
        }
        lightsBuffer.rewind();        
        GL20.glUniform1(lightsLocation, lightsBuffer);         
        

        // Bind to the VAO that has all the information about the vertices
        GL30.glBindVertexArray(vaoId);
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
        GL20.glEnableVertexAttribArray(3);
        GL20.glEnableVertexAttribArray(4);
        GL20.glEnableVertexAttribArray(5);

        // Bind to the index VBO that has all the information about the order of the vertices
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);

        // Draw the vertices
        GL11.glDrawElements(GL11.GL_TRIANGLES,meshCreator.getElements().limit(), GL11.GL_UNSIGNED_INT, 0);
       
        // Put everything back to default (deselect)
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL20.glDisableVertexAttribArray(3);
        GL20.glDisableVertexAttribArray(4);
        GL20.glDisableVertexAttribArray(5);
        GL30.glBindVertexArray(0);
        GL20.glUseProgram(0);

        // Force a maximum FPS of about 60
        Display.sync(60);
        // Let the CPU synchronize with the GPU if GPU is tagging behind
        Display.update();
    }

    public void destroy() {
        // Delete the shaders
        GL20.glUseProgram(0);
        GL20.glDetachShader(pId, vsId);
        GL20.glDetachShader(pId, fsId);

        GL20.glDeleteShader(vsId);
        GL20.glDeleteShader(fsId);
        GL20.glDeleteProgram(pId);

        // Select the VAO
        GL30.glBindVertexArray(vaoId);

        // Disable the VBO index from the VAO attributes list
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL20.glDisableVertexAttribArray(3);
        GL20.glDisableVertexAttribArray(4);
        GL20.glDisableVertexAttribArray(5);

        // Delete the vertex VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboId);

        // Delete the index VBO
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboiId);

        // Delete the VAO
        GL30.glBindVertexArray(0);
        GL30.glDeleteVertexArrays(vaoId);

        Display.destroy();
    }

    private void setupDisplay() {
        // Setup an OpenGL context with API version 3.2
        try {
            PixelFormat pixelFormat = new PixelFormat();
            ContextAttribs contextAtrributes = new ContextAttribs(3, 2)
                    .withForwardCompatible(true)
                    .withProfileCore(true);

            Display.setDisplayMode(Display.getDesktopDisplayMode());
            Display.setFullscreen(true);
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create(pixelFormat, contextAtrributes);
            GL11.glEnable(GL11.GL_DEPTH_TEST);


        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // Setup an XNA like background color
        GL11.glClearColor(0f, 0f, 0f, 0f);

        displayWidth = Display.getWidth();
        displayHeight = Display.getHeight();
        // Map the internal OpenGL coordinate system to the entire screen        
        GL11.glViewport(0, 0, displayWidth, displayHeight);
    }

    private void setupMesh(Function f, float step) {

        meshCreator= new MeshCreator();
        meshCreator.generateBuffers(f,step);
        // Create a new Vertex Array Object in memory and select it (bind)
        // A VAO can have up to 16 attributes (VBO's) assigned to it by default
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);

        // Create a new Vertex Buffer Object in memory and select it (bind)
        // A VBO is a collection of Vectors which in this case resemble the location of each vertex.
        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);

        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, meshCreator.getVerticies(), GL15.GL_STATIC_DRAW);
        // Put the position coordinates in attribute list 0
        GL20.glVertexAttribPointer(0, Function.DataSizes.positionNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.positionByteOffset);
        // Put the normal coordinates in attribute list 1
        GL20.glVertexAttribPointer(1, Function.DataSizes.normalNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.normalByteOffset);
        //Material attributes setup:
        GL20.glVertexAttribPointer(2, Function.DataSizes.ambientNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.ambientByteOffset);
        GL20.glVertexAttribPointer(3, Function.DataSizes.diffuseNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.diffuseByteOffset);
        GL20.glVertexAttribPointer(4, Function.DataSizes.specularNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.specularByteOffset);
        GL20.glVertexAttribPointer(5, Function.DataSizes.shininessNumbersCount, GL11.GL_FLOAT,
                false, Function.DataSizes.totalSizeInBytes, Function.DataSizes.shininessByteOffset);
        
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);

        // Create a new VBO for the indices and select it (bind)
        vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, meshCreator.getElements(), GL15.GL_STATIC_DRAW);
        // Deselect (bind to 0) the VBO
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    private void setupShaders() {
        int errorCheckValue = GL11.glGetError();

        System.out.println("Classpath: " + this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        // Load the vertex shader
        vsId = loadShader("shaders/vertex.glsl", GL20.GL_VERTEX_SHADER);
        // Load the fragment shader
        fsId = loadShader("shaders/fragment.glsl", GL20.GL_FRAGMENT_SHADER);

        // Create a new shader program that links both shaders
        pId = GL20.glCreateProgram();
        GL20.glAttachShader(pId, vsId);
        GL20.glAttachShader(pId, fsId);

        // Position information will be attribute 0
        GL20.glBindAttribLocation(pId, 0, "in_Position");
        // Normal information will be attribute 1
        GL20.glBindAttribLocation(pId, 1, "in_Normal");
        //Material information
        GL20.glBindAttribLocation(pId, 2, "in_Ambient");
        GL20.glBindAttribLocation(pId, 3, "in_Diffuse");
        GL20.glBindAttribLocation(pId, 4, "in_Specular");
        GL20.glBindAttribLocation(pId, 5, "in_Shininess");
       
        GL20.glLinkProgram(pId);
        GL20.glValidateProgram(pId);
        
         // Get matrices uniform locations        
        modelMatrixLocation = GL20.glGetUniformLocation(pId, "model");
        viewMatrixLocation = GL20.glGetUniformLocation(pId, "view");
        projectionMatrixLocation = GL20.glGetUniformLocation(pId, "projection");
        normalTransformMatrixLocation = GL20.glGetUniformLocation(pId, "normalTransMatrix");
        lightsNumberLocation = GL20.glGetUniformLocation(pId, "lightsNumber");
        lightsLocation=GL20.glGetUniformLocation(pId, "lightsArray");
        
        errorCheckValue = GL11.glGetError();
        if (errorCheckValue != GL11.GL_NO_ERROR) {
            System.out.println("ERROR - Could not create the shaders:" + GLU.gluErrorString(errorCheckValue));
            System.exit(-1);
        }
    }

    private int loadShader(String filename, int type) {
        StringBuilder shaderSource = new StringBuilder();
        int shaderID = 0;
        File currentPath = new File(".");
        currentPath = currentPath.getAbsoluteFile().getParentFile();
        System.out.println(currentPath);
        //filename=currentPath.getAbsolutePath()+filename;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = reader.readLine()) != null) {
                shaderSource.append(line).append("\n");
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Could not read file.");
            e.printStackTrace();
            System.exit(-1);
        }

        shaderID = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderID, shaderSource);
        GL20.glCompileShader(shaderID);

        return shaderID;
    }
}
