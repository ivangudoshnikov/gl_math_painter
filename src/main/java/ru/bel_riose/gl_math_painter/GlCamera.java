/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Bel_Riose
 */
 public class GlCamera {

        private float fieldOfView;
        private float aspectRatio;
        private float near_plane;
        private float far_plane;
        public Vector3f cameraPos, up, target;
        private Matrix4f view, projection;

        public GlCamera(float fieldOfView, float aspectRatio, float near_plane, float far_plane, Vector3f cameraPos, Vector3f up, Vector3f target) {
            this.fieldOfView = fieldOfView;
            this.aspectRatio = aspectRatio;
            this.near_plane = near_plane;
            this.far_plane = far_plane;
            this.cameraPos = cameraPos;
            this.up = up;
            this.target = target;
            generateProjectionMatrix();
            generateViewMatrix();
        }

        public Matrix4f getView() {
            return new Matrix4f(view);
        }

        public Matrix4f getProjection() {
            return new Matrix4f(projection);
        }

        private void generateProjectionMatrix() {
            projection = new Matrix4f();
            float y_scale = (float) (1 / Math.tan(Utils.degreesToRadians(fieldOfView / 2f)));
            float x_scale = y_scale / aspectRatio;
            float frustum_length = far_plane - near_plane;
            projection.m00 = x_scale;
            projection.m11 = y_scale;
            projection.m22 = -((far_plane + near_plane) / frustum_length);
            projection.m23 = -1;
            projection.m32 = -((2 * near_plane * far_plane) / frustum_length);
            projection.m33 = 0;
        }

        public void generateViewMatrix() {
            Vector3f u = new Vector3f(), v = new Vector3f(), w = new Vector3f();
            //Calculating w
            Vector3f.sub(cameraPos, target, w);
            w.normalise();
            //Calculating u
            Vector3f.cross(up, w, u);
            u.normalise();
            //Calculating v
            Vector3f.cross(w, u, v);
            view = new Matrix4f();
            view.m00 = u.x;
            view.m10 = u.y;
            view.m20 = u.z;
            view.m30 = -Vector3f.dot(u, cameraPos);
            view.m01 = v.x;
            view.m11 = v.y;
            view.m21 = v.z;
            view.m31 = -Vector3f.dot(v, cameraPos);
            view.m02 = w.x;
            view.m12 = w.y;
            view.m22 = w.z;
            view.m32 = -Vector3f.dot(w, cameraPos);
            view.m03 = 0;
            view.m13 = 0;
            view.m23 = 0;
            view.m33 = 1;
        }
    }
