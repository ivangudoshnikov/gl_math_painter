/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

/**
 *
 * @author Bel_Riose
 */
class BadRectangleException extends Exception {
}

public class Rectangle {

    float x0, y0, x1, y1, x0_true, y0_true, x1_true, y1_true;
    //Include of exclude?
    boolean inc_x0, inc_y0, inc_x1, inc_y1;

    /**
     *
     * @param x0
     * @param inc_x0
     * @param x1
     * @param inc_x1
     * @param y0
     * @param inc_y0
     * @param y1
     * @param inc_y1
     * @throws BadRectangleException
     */
    public Rectangle(float x0, boolean inc_x0, float x1, boolean inc_x1, float y0, boolean inc_y0, float y1, boolean inc_y1) throws BadRectangleException {
        this.x0 = x0;
        this.inc_x0 = inc_x0;
        this.x1 = x1;
        this.inc_x1 = inc_x1;
        this.y0 = y0;
        this.inc_y0 = inc_y0;
        this.y1 = y1;
        this.inc_y1 = inc_y1;

        if (inc_x0) {
            x0_true = x0;
        } else {
            x0_true = Math.max(Math.nextUp(x0), x0 + 2*Math.nextUp(1f / Float.MAX_VALUE));
        }
        if (inc_y0) {
            y0_true = y0;
        } else {
            y0_true = Math.max(Math.nextUp(y0), y0 + 2*Math.nextUp(1f / Float.MAX_VALUE));
        }
        if (inc_x1) {
            x1_true = x1;
        } else {
            x1_true = Math.min(Math.nextAfter(x1, x1 - 1), x1 - 2*Math.nextUp(1f / Float.MAX_VALUE));
        }
        if (inc_y1) {
            y1_true = y1;
        } else {
            y1_true = Math.min(Math.nextAfter(y1, y1 - 1), y1 - 2*Math.nextUp(1f / Float.MAX_VALUE));
        }
        if ((x1_true < x0_true) || (y1_true < y0_true)) {
            throw new BadRectangleException();
        }

    }
}
