/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import org.lwjgl.util.vector.*;

/**
 *
 * @author Bel_Riose
 */
public class LightSource {
    public static final int numberCount=8;
    Vector4f pos,color;
    public LightSource(float x,float y,float z,float w,float r,float g,float b,float a){
        pos = new Vector4f(x, y, z, w);
        color=new Vector4f(r, g, b, a);
}
    public float[] getTransformedData(Matrix4f view){
        Vector4f viewPos=new Vector4f();
        Matrix4f.transform(view, pos, viewPos);
                
        
        //viewPos=pos;
        
        return new float[]{viewPos.x,viewPos.y,viewPos.z,viewPos.w,color.x,color.y,color.z,color.w};
    }
    
}
