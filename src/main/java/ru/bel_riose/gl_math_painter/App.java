package ru.bel_riose.gl_math_painter;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.*;

public class App {

    private static GlRenderer renderer;

    private static void up(float t) {
        Vector3f axis = new Vector3f();
        Vector3f.cross(renderer.glCamera.cameraPos, renderer.glCamera.up, axis);
        Matrix3f m = MathUtils.rotation3d(axis, t);
        Matrix3f.transform(m, renderer.glCamera.cameraPos, renderer.glCamera.cameraPos);
        Matrix3f.transform(m, renderer.glCamera.up, renderer.glCamera.up);
        renderer.glCamera.generateViewMatrix();
    }

    private static void left(float t) {
        Matrix3f m = MathUtils.rotation3d(renderer.glCamera.up, t);
        Matrix3f.transform(m, renderer.glCamera.cameraPos, renderer.glCamera.cameraPos);
        renderer.glCamera.generateViewMatrix();
    }

    private static void roll(float t) {
        Matrix3f m = MathUtils.rotation3d(renderer.glCamera.cameraPos, t);
        Matrix3f.transform(m, renderer.glCamera.up, renderer.glCamera.up);
        renderer.glCamera.generateViewMatrix();
    }

    private static void zoom(float s) {
        renderer.glCamera.cameraPos.scale(s);
        renderer.glCamera.generateViewMatrix();
    }

    // Entry point for the application
    public static void main(String[] args) throws BadRectangleException, IntersectingDomainException {
        UserConfig config = new UserConfig();
        renderer = new GlRenderer("GL Math Painter");
        renderer.init(config.glCamera,config.function, config.step, config.lights);

        boolean running = true;

        while (running) {
            // Do a single loop (logic/render)
            renderer.render();

            if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                running = false;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                up((float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
                up(-(float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
                left((float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                left(-(float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
                roll(-(float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
                roll((float) Math.PI / 48f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_ADD)) {
                zoom(1 / 1.05f);
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_SUBTRACT)) {
                zoom(1.05f);
            }           

            running = running && !Display.isCloseRequested();
        }
        renderer.destroy();
    }
}
