/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.gl_math_painter;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.AbstractQueue;
import java.util.PriorityQueue;
import java.util.Queue;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.*;

/**
 *
 * @author Bel_Riose
 */
public class MeshCreator {

    private FloatBuffer verticies;
    private IntBuffer elements;

    public void generateBuffers(Function f, float user_step) {
        float[] du = new float[f.domain.length];
        float[] dv = new float[f.domain.length];

        int[] nu = new int[f.domain.length];
        int[] nv = new int[f.domain.length];

        float[] step_u = new float[f.domain.length];
        float[] step_v = new float[f.domain.length];

        int verticies_number = 0, triangles_number = 0;

        for (int domain_part = 0; domain_part < f.domain.length; domain_part++) {
            //distances
            du[domain_part] = Math.abs(f.domain[domain_part].x1_true - f.domain[domain_part].x0_true);
            dv[domain_part] = Math.abs(f.domain[domain_part].y1_true - f.domain[domain_part].y0_true);
            //numbers of steps
            nu[domain_part] = (int) (du[domain_part] / user_step);
            nv[domain_part] = (int) (dv[domain_part] / user_step);
            //precise steps
            step_u[domain_part] = du[domain_part] / nu[domain_part];
            step_v[domain_part] = dv[domain_part] / nv[domain_part];
            //we have (nu+1)*(nv+1) "corner" verticies andn nu*nv "central" verticies
            verticies_number += (nu[domain_part] + 1) * (nv[domain_part] + 1) + nu[domain_part] * nv[domain_part];
            //for every square we have 4 triangles
            triangles_number += 4 * nu[domain_part] * nv[domain_part];
        }

        verticies = BufferUtils.createFloatBuffer(verticies_number * Function.DataSizes.totalNumbersCount);
        elements = BufferUtils.createIntBuffer(triangles_number * 3);

        Queue<Integer> elements_queue = new PriorityQueue<Integer>(nu[0] + 1);
        //verticies counter
        int counter = 0;
        for (int k = 0; k < f.domain.length; k++) {
            //filling lower row
            for (int i = 0; i <= nu[k]; i++) {
                verticies.put(f.getData(f.domain[k].x0_true + i * step_u[k], f.domain[k].y0_true));                
                elements_queue.add(counter);
                counter++;
            }
            for (int j = 0; j < nv[k]; j++) {
                verticies.put(f.getData(f.domain[k].x0_true, f.domain[k].y0_true + (j + 1) * step_v[k]));
                elements_queue.add(counter);
                int leftTopVertex = counter;
                counter++;

                for (int i = 0; i < nu[k]; i++) {
                    //central vertex:
                    verticies.put(f.getData(f.domain[k].x0_true + (i + 0.5f) * step_u[k], f.domain[k].y0_true + (j + 0.5f) * step_v[k]));                    
                    int central = counter;
                    counter++;

                    //Lower triangle
                    int leftBottomVertex = elements_queue.remove();
                    elements.put(central);
                    elements.put(leftBottomVertex);
                    elements.put(elements_queue.element());

                    //Left triangle
                    elements.put(central);
                    elements.put(leftTopVertex);
                    elements.put(leftBottomVertex);

                    //Top triangle
                    elements.put(central);
                    verticies.put(f.getData(f.domain[k].x0_true + (i + 1) * step_u[k], f.domain[k].y0_true + (j + 1) * step_v[k]));
                    elements_queue.add(counter);
                    elements.put(counter);
                    int rightTopVertex = counter;
                    counter++;
                    elements.put(leftTopVertex);

                    //Right triangle 
                    elements.put(central);
                    elements.put(elements_queue.element());
                    elements.put(rightTopVertex);

                    //for next square:
                    leftTopVertex = rightTopVertex;
                }
                elements_queue.remove();
            }
            elements_queue.clear();
        }
        elements.flip();
        verticies.flip();
    }

    public IntBuffer getElements() {
        return elements;
    }

    public FloatBuffer getVerticies() {
        return verticies;
    }
}
